import img1 from '../img/scrapping.png';

export const FAKE_DB = {
    perfil: {
        name: "Jose Manuel Vazquez Rojas",
        profession: "Frontend Developer",
        mobile: "+52 271 263-4022",
        email: "manuel1998angel@outlook.com",
    },
    curriculumSections: {
        education: [
            {
                title: "Frontend developer",
                year: 2023,
                description: ""
            },
            {
                title: "Project Manager",
                year: 2022,
                description: ""
            }
        ],
        experience: [
            {
                title: "Wed Developer",
                year: 2023,
                description: ""
            }
        ]
    },
    portafolio: [
        {
            url: img1,
            alt: "scrappi"
        },

    ],
    footer: {
        info: "Code with passion ",
        email: "manuel1998angel@outlook.com"
    },
}